import os, cv2
import random
from sklearn.metrics import confusion_matrix
import numpy as np
from PIL import Image , ImageOps  
from numpy import asarray
from tensorflow.keras.layers import Input,Lambda,Dense,Flatten,GlobalMaxPool2D,Dropout,MaxPool2D,Conv2D
from tensorflow.keras.models import Model,Sequential
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg16 import preprocess_input
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
from glob import glob
import matplotlib.pyplot as plt




train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

valid_datagen = ImageDataGenerator(rescale = 1./255)



training_set = train_datagen.flow_from_directory(r'/home/ubuntu/tm/train',
                                                 target_size = (224, 224),
                                                 batch_size = 32,
                                                 class_mode = 'categorical')

validation_set = valid_datagen.flow_from_directory(r'/home/ubuntu/tm/validation',
                                            target_size = (224, 224),
                                            batch_size = 32,
                                            class_mode = 'categorical')


img_size=[224,224]
vgg = VGG16(input_shape=img_size + [3], weights='imagenet', include_top=False)
for layer in vgg.layers:
  layer.trainable = False
x = Flatten()(vgg.output)
prediction = Dense(2, activation='softmax')(x)
model = Model(inputs=vgg.input, outputs=prediction)
model.compile(
  loss='binary_crossentropy',
  optimizer='adam',
  metrics=['accuracy']
)

model.compile(
  loss='categorical_crossentropy',
  optimizer='adam',
  metrics=['accuracy']
)
r = model.fit(
  training_set,
  validation_data=validation_set,
  epochs=7,
  steps_per_epoch=len(training_set),
  validation_steps=len(validation_set)
)


img_size=224
datadir=r"/home/ubuntu/tm/test"
training_data=[]
categories=["Male","Female"]
for category in categories:
    path=os.path.join(datadir,category)
    class_num=categories.index(category)
    for img in os.listdir(path):
        try:
            image = cv2.imread(os.path.join(path,img))
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = cv2.resize(image, (img_size, img_size))
            training_data.append([image, class_num])
        except Exception as e:
            pass
random.shuffle(training_data)
x_test=[]
y_test=[]
for image,gender in training_data:
    x_test.append(image)
    y_test.append(gender)
X_train=[]
for i in range(len(x_test)):
    img_array=asarray(x_test[i])
    X_train.append(img_array)
x_test=X_train
x_test=np.array(x_test)
x_test=x_test.reshape(-1,img_size,img_size,3)
y_pred=model.predict(x_test)
y_pred_final=[]


for i in range(len(y_pred)):
    y_pred_final.append(y_pred[i][0])
y_pred_final=np.round(y_pred_final)
from sklearn.metrics import confusion_matrix
cm=confusion_matrix(y_pred_final,y_test)
model_accuracy=(cm[0][0]+cm[1][1]) / ( cm[1][0] + cm[0][1]+cm[0][0]+cm[1][1])
print(model_accuracy )
model.save(r'home/ubuntu/tm/model7.h5')


